# Complete the calculate_grade function which accepts
# a list of numerical scores each between 0 and 100.
#
# Based on the average of the scores, the function
# returns
#   * An "A" for an average greater than or equal to 90
#   * A "B" for an average greater than or equal to 80
#     and less than 90
#   * A "C" for an average greater than or equal to 70
#     and less than 80
#   * A "D" for an average greater than or equal to 60
#     and less than 70
#   * An "F" for any other average

def calculate_grade(values):
    total = sum(values)
    average = total // (len(values))
    if average >= 90:
        return "A"
    if 80 <= average <= 90:
        return "B"
    if 70 <= average <= 80:
        return "C"
    if 60 <= average <= 70:
        return "D"
    else:
        return "F"
scores = [90, 70, 90, 80]
average_score = calculate_grade(scores)
print(average_score)
