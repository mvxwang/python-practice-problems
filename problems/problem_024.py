# Complete the calculate_average function which accepts
# a list of numerical values and returns the average of
# the numbers.
#
# If the list of values is empty, the function should
# return None
#
# Pseudocode is available for you

def calculate_average(values):
    total = 0
    if len(values) == 0:
        return None
    for nums in values:
        total += nums

    average = (total // (len(values)))
    return average

list1 = [100, 100, 100, 100]
ans = calculate_average(list1)
print(ans)
